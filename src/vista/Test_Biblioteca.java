package vista;
import mundo.*;


/**
 * Write a description of class Test_Biblioteca here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Test_Biblioteca
{
    // instance variables - replace the example below with your own
    
    public static void main(String args[])
    {
        String url1="https://gitlab.com/madarme/archivos-persistencia/raw/master/facultad.csv";
        String url2="https://gitlab.com/madarme/archivos-persistencia/raw/master/inventario.csv";
        String url3="https://gitlab.com/madarme/archivos-persistencia/raw/master/estudiantes.csv";
        String url4="https://gitlab.com/madarme/archivos-persistencia/raw/master/solicitudes.csv";
        Biblioteca b=new Biblioteca("Cote Lemus", url1, url2,url3,url4);
//        System.out.println(b.getListadoFacultad());
//        System.out.println(b.getListadoEstudiantes());
        //Después de cargado los archivos de texto, se debe procesar:
        b.procesarSolicitudes();
        //Imprimir los mensajes del sistema:
        System.out.println(b.getListadoMensajes());
    }
  
    
}
