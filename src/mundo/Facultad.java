package mundo;

import util.*;

/**
 * Write a description of class Facultad here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Facultad {
    // instance variables - replace the example below with your own

    private String nombre;
    private int codigo;
    Pila<Libro> libros = new Pila();

    /**
     * Constructor for objects of class Facultad
     */
    public Facultad() {
        // initialise instance variables

    }

    //lo agregue
    public boolean pertenece(String l) {

        if (l.charAt(0) == codigo) {
            return true;
        }

        return false;
    }

    //lo agregue
    public boolean existe(String l) {

        Pila<Libro> aux = new Pila<>();

        while (!libros.esVacia()) {
            Libro cod = libros.getTope();
            if (codigo == cod.getCodigo()) {
                vaciar(aux);
                return true;
            }

            aux.apilar(libros.desapilar());
        }
        vaciar(aux);
        return false;

    }

    //lo agregue
    public Libro eliminarLibro(int codigo) {

        Pila<Libro> aux = new Pila<>();

        while (!libros.esVacia()) {
            Libro cod = libros.getTope();

            if (codigo == cod.getCodigo()) {
                vaciar(aux);
                return libros.desapilar();
            }

            aux.apilar(libros.desapilar());
        }

        vaciar(aux);
        return null;
    }

    //lo agregue
    private void vaciar(Pila<Libro> L) {
        while (!L.esVacia()) {
            libros.apilar(L.desapilar());
        }
    }

   
    public Facultad(String nombre, int cod) {
        this.nombre = nombre;
        this.codigo = cod;

        // initialise instance variables
    }

    //Start GetterSetterExtension Source Code
    /**
     * GET Method Propertie nombre
     */
    public String getNombre() {
        return this.nombre;
    }//end method getNombre

    /**
     * SET Method Propertie nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }//end method setNombre

    /**
     * GET Method Propertie codigo
     */
    public int getCodigo() {
        return this.codigo;
    }//end method getCodigo

    /**
     * SET Method Propertie codigo
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }//end method setCodigo

    //End GetterSetterExtension Source Code
//!
    public String toString() {
        String m = this.getListadoLibros();
        return ("Codigo:" + this.getCodigo() + "\t Nombre Fac:" + this.getNombre() + "\n" + m);
    }

    public void addLibro(Libro l) {
        this.getLibros().apilar(l);
    }

    public Libro getLibro(Libro l) {
        Libro libro = null;
        Pila<Libro> libroCopia = new Pila<>();
        while (!libros.esVacia()) {
            Libro tmp = libros.desapilar();
            if (tmp.equals(l)) {
                libro = tmp;
            } else {
                libroCopia.apilar(tmp);
            }
        }
        libros = libroCopia;
        return libro;
    }

    //Start GetterSetterExtension Source Code
    /**
     * GET Method Propertie libros
     */
    public util.Pila<mundo.Libro> getLibros() {
        return this.libros;
    }//end method getLibros

    public String getListadoLibros() {
        String m = "";
        while (!this.libros.esVacia()) {
            //Esto es un libro dentro de la pila: this.libros.desapilar().
            m += this.libros.desapilar().toString() + "\n";

        }
        return m;
    }

    //End GetterSetterExtension Source Code
//!
}
