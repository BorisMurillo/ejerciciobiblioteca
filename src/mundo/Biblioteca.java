package mundo;
import util.*;
import util_archivo.*;


/**
 * Write a description of class Biblioteca here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Biblioteca
{
    // instance variables - replace the example below with your own
    private String nombre;
    private ListaCD<Facultad> facultades=new ListaCD();
    private ListaCD<Estudiante> estudiantes=new ListaCD();
    private ListaCD<Mensaje> mensajes=new ListaCD();
    private ColaP<Solicitud> solicitudes=new ColaP();
    

    /**
     * Constructor for objects of class Biblioteca
     */
    public Biblioteca()
    {
        // initialise instance variables
        
    }
    
    
    
    
    //lo agregue
    private void reservarLibro(int codLibro){
        String cod=codLibro+"";
     for(Facultad f:facultades)
     {
      if(f.pertenece(cod) && f.existe(cod))
         f.eliminarLibro(codLibro);    
     }
        
    }
    
    //lo agregue
    private void entregarLibro(Libro l ){
    for(Facultad f:facultades)
    {    
    if(f.pertenece(l.getCodigo()+""))
        f.addLibro(l);     
        }
    
    }
    
    public Biblioteca(String nombre, String urlFac, String urlLibros, String urlEstd,String urlSol)
    {
        // initialise instance variables
        this.nombre=nombre;
        //Crear facultades:
        this.crearFacultad(urlFac);
        //Crear Libros
        this.crearLibros(urlLibros);
        //Crear Estudiantes
        this.crearEstudiantes(urlEstd);
        //Crear Solicitudes
        this.crearSolicitudes(urlSol);
        
    }
    
    
    private void crearSolicitudes(String urlSol)
    {
        ArchivoLeerURL file = new ArchivoLeerURL(urlSol);
        Object v[] = file.leerArchivo();
               
        for(Integer i=1; i<v.length; i++){           
            String datos[] = v[i].toString().split(";");
            Integer codLibro = Integer.parseInt(datos[0]);
            Integer tipoServicio = Integer.parseInt(datos[1]);
            Integer codEst = Integer.parseInt(datos[2]);
            
            Estudiante estudiante = this.existe(codEst);
            if(estudiante!=null){ 
                Solicitud solicitud = new Solicitud(estudiante,new Libro(codLibro, ""),tipoServicio);
                solicitudes.enColar(solicitud, 100-estudiante.getSemestre());               
            }else{
                mensajes.insertarAlFinal(new Mensaje("El código del estudiante : "+codEst+" es inválido"));
            }
        }
    }
    
    private Estudiante existe(Integer cod){
        Estudiante est = new Estudiante();
        est.setCodigo(cod);
        for(Integer i=0; i<estudiantes.getTamanio(); i++){
            if(estudiantes.get(i).equals(est)){
                return estudiantes.get(i);
            }
        }
        return null;
    }
    
    private Boolean libroValido(Libro libro){
        Integer codigo = libro.getCodigo()/100000;
        Integer cont = 0;
        Integer x = libro.getCodigo();
        while(x!=0){
            x/=10;
            cont++;
        }
        return codigo>0 && codigo<=this.facultades.getTamanio() && cont==6;
    }
    
    private Libro getLibro(Libro l){
        for(Integer i=0; i<facultades.getTamanio(); i++){
            Libro libro = facultades.get(i).getLibro(l);
            if(libro!=null){
                return libro;
            }
        }
        return null;
    }
    
    public void procesarSolicitudes()
    {       
        while(!solicitudes.esVacia()){           
            Solicitud s = solicitudes.deColar(); 
            if(existe(s.getEst_desconocido().getCodigo())==null)//esto lo agregue
                guardarMensaje(1, s.getEst_desconocido(), s.getLibro_desconocido());//tambien
                
            if(!libroValido(s.getLibro_desconocido())){
                guardarMensaje(2, s.getEst_desconocido(), s.getLibro_desconocido());
                continue;//Si el libro no es valido directamente paso a la siguiente iteracion, pero no sin antes
                         //haber guardado su respectivo mensaje
            }
            if(s.getTipo_servicio()==1){                      
                if(getLibro(s.getLibro_desconocido())!=null){
                            
                    if(s.getEst_desconocido().reservar()){
                        reservarLibro(s.getLibro_desconocido().getCodigo());//lo agregue
                        guardarMensaje(4, s.getEst_desconocido(), s.getLibro_desconocido());
                    }else{
                        guardarMensaje(6, s.getEst_desconocido(), s.getLibro_desconocido());
                    }  
                            
                }else{
                    guardarMensaje(5, s.getEst_desconocido(), s.getLibro_desconocido());
                }                   
            }else{
                Integer cod = (s.getLibro_desconocido().getCodigo()/100000)-1;
                this.facultades.get(cod).addLibro(s.getLibro_desconocido()); 
                entregarLibro(s.getLibro_desconocido());//lo agregue
                guardarMensaje(3, s.getEst_desconocido(), s.getLibro_desconocido()); 
            }
        }
    }
    
    private void guardarMensaje(Integer x, Estudiante datoEst, Libro datoLib){
        switch(x){
            case 1 : mensajes.insertarAlFinal(new Mensaje("El código del estudiante : "+datoEst.getCodigo()+" es inválido")); 
            break;
            case 2 : mensajes.insertarAlFinal(new Mensaje("El código del libro : "+datoLib.getCodigo()+" es inválido"));
            break;
            case 3 : mensajes.insertarAlFinal(new Mensaje("Se entregó el libro con código: "+datoLib.getCodigo()+" del estudiante de código: "+datoEst.getCodigo()));
            break;
            case 4 : mensajes.insertarAlFinal(new Mensaje("Se reservó el libro con código: "+datoLib.getCodigo()+" al estudiante de código: "+datoEst.getCodigo()));
            break;
            case 5 : mensajes.insertarAlFinal(new Mensaje("No se reservó el libro con código "+datoLib.getCodigo()+" no se encontraba en el inventario"));
            break;
            case 6 : mensajes.insertarAlFinal(new Mensaje("No se reservó el libro con código "+datoLib.getCodigo()+" POR QUE el estudiante con código: "+datoEst.getCodigo()+" excedió su cupo"));
            break;
        }
    }
    
    public String getListadoMensajes()
    {
        return (this.mensajes.toString());
    }
    
    
    private void crearEstudiantes(String urlEstd)
    {
    ArchivoLeerURL file=new ArchivoLeerURL(urlEstd);
    Object v[]=file.leerArchivo();
    for(int i=1; i<v.length;i++)
        {
            String fila=v[i].toString();
            String datos[]=fila.split(";");
            int cod=Integer.parseInt(datos[0]);
            int sem=Integer.parseInt(datos[3]);
            String nombre=datos[1];
            String email=datos[2];
            //Crear Estudiante:
            Estudiante nuevo=new Estudiante(cod, nombre, email, sem);
            //adicionar este estudiante a la lista de estudiantes:
            this.estudiantes.insertarAlFinal(nuevo);
            
        }
    
    
    
    }
    
    
    
    
    
    private void crearFacultad(String urlFac)
    {
    ArchivoLeerURL file=new ArchivoLeerURL(urlFac);
    Object v[]=file.leerArchivo();
    
        for(int i=1; i<v.length;i++)
        {
            String fila=v[i].toString();
            String datos[]=fila.split(";");
            int cod=Integer.parseInt(datos[0]);
            String nombre=datos[1];
            //Crear facultad:
            Facultad f=new Facultad(nombre,cod);
            this.facultades.insertarAlFinal(f);
        }
        
    }
    
    private void crearLibros(String urlLibros)
    {
    ArchivoLeerURL file=new ArchivoLeerURL(urlLibros);
        Object v[]=file.leerArchivo();
    
        for(int i=1; i<v.length;i++)
        {
            String fila=v[i].toString();
            String datos[]=fila.split(";");
            int cod=Integer.parseInt(datos[0]);
            String nombre=datos[1];
            //Crear facultad:
            Libro l=new Libro(cod,nombre);
            int primer=cod/100000;
            
            this.facultades.get(primer-1).addLibro(l);
            

        }
        
    }
    
    
    public String getListadoEstudiantes()
    {
        return this.estudiantes.toString();
    }
    
    
    
    
    public String getListadoFacultad()
    {
       
        return this.facultades.toString();
    
    }
    

    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    
}
