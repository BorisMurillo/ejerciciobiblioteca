package mundo;


/**
 * Write a description of class Libro here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Libro
{
    // instance variables - replace the example below with your own
    private int codigo;
    private String nombre;

    /**
     * Constructor for objects of class Libro
     */
    public Libro()
    {
        // initialise instance variables
    
    }
    
    public Libro(int cod, String nombre)
    {
        // initialise instance variables
        this.codigo=cod;
        this.nombre=nombre;
    
    }

    

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie codigo*/
    public int getCodigo(){
        return this.codigo;
    }//end method getCodigo

    /**SET Method Propertie codigo*/
    public void setCodigo(int codigo){
        this.codigo = codigo;
    }//end method setCodigo

    /**GET Method Propertie nombre*/
    public String getNombre(){
        return this.nombre;
    }//end method getNombre

    /**SET Method Propertie nombre*/
    public void setNombre(String nombre){
        this.nombre = nombre;
    }//end method setNombre

    //End GetterSetterExtension Source Code
//!

    public String toString()
    {
        return ("Cod:"+this.codigo+"\t Nombre Libro:"+this.getNombre());
    }

    public boolean equals(Object c)
    {
        Libro x=(Libro)c;
        return x.getCodigo()==this.getCodigo();
    }
    
    //Start GetterSetterExtension Source Code
    //End GetterSetterExtension Source Code
//!
}
